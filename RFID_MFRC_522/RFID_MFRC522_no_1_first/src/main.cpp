#include <Arduino.h>
#include <MFRC522.h>
#include <SPI.h>
#include <Keypad.h>
#include <LiquidCrystal.h>
#include <String.h>

#define RST_PIN 32
#define SDA_PIN 23

MFRC522 mfrc522(SDA_PIN, RST_PIN);
const byte ROWS = 4;
const byte COLMS = 4;

char keys[ROWS][COLMS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

  byte rowPins[ROWS] = {17, 16, 4, 12};
  byte colPins[COLMS] = {14, 22, 11, 8};
  int cnt = 0;

  Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLMS );
  LiquidCrystal lcd(10, 13, 5, 2, 6, 7);



  struct pigeon {
    String number;
    long rid;
  } Taube[500];

  void zuordnen()
  {
    if(! mfrc522.PICC_IsNewCardPresent())
    {
      return;
    }

    if(! mfrc522.PICC_ReadCardSerial())
    {
      return;
    }

    lcd.setCursor(0, 0);
    lcd.print("Warte auf Ring...");

    for(byte i = 0; i < mfrc522.uid.size; i++)
    {
      Taube[cnt].rid = ((Taube[cnt].rid+mfrc522.uid.uidByte[i])*10);
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ID: ");
      lcd.print(Taube[cnt].rid);
      cnt++;
    }

  }

  void numeing()
  {
    if (kpd.getKeys())
      {
        char key = kpd.getKey();

          switch (key) {
            case '0' || '1' || '2' || '3' || '4' || '5' || '6' || '7' || '8' || '9':
            lcd.setCursor(0, 1);
            lcd.print(key);
            Taube[cnt - 1].number += key;
            break;

            case 'B':
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print(Taube[cnt - 1].number);
            lcd.setCursor(0, 1);
            lcd.print("wurde zugeordnet");
            break;

            default:
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("unbekanntes");
            lcd.setCursor(0, 1);
            lcd.print("Zeichen:");
            lcd.print(key);
          }
      }
  }

void setup() {

Serial.begin(9600);
SPI.begin();
mfrc522.PCD_Init();
lcd.begin(16, 2);

}

void loop() {
  zuordnen();
  numeing();
}


/*String numeing()
{
  for(int b = 0; b<=10; b++)
  {
    char key = kpd.getKey();
    lcd.print(key);
    strcat(Taube[cnt].number, key);
  }
}*/

/*  if(! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }

  if(! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }
*/

/*Serial.print("RFID TAG ID (HEX) lautet: ");

  for(byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i],HEX);
    Serial.print("");
  }*/
